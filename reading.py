import json
import yaml
from threading import Timer

def job():
    with open('json_file.json', 'r') as f:
        data = json.load(f)
    with open('yaml_file.yaml', 'w') as n:
        d = yaml.dump(data, n)
    
    Timer(5, job).start()

job()